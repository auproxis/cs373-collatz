#!/usr/bin/env python4

# ----------
# Collatz.py
# ----------

# pylint: disable = bad-whitespace
# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# -------
# imports
# -------

from typing import IO, List

# ------------
# collatz_read
# ------------

answers: List[int] = [-1] * 1000000
answers[1] = 1


def collatz_read(s: str) -> List[int]:
    """
    read two ints
    s a string
    return a list of two ints, representing the beginning and end of a range, [i, j]
    """
    a = s.split()
    return [int(a[0]), int(a[1])]


# ------------
# collatz_eval
# ------------


def collatz_eval(i: int, j: int) -> int:
    """
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    return the max cycle length of the range [i, j]
    """
    if i > j:
        i, j = j, i

    m = (j // 2) + 1

    if i < m:
        i = m

    vals: List[int] = []

    for num in range(i, j + 1):
        vals.append(collatz_single(num))
    return max(vals)


def collatz_single(i: int) -> int:
    # retrieve from dict if answer cache'd
    if i < len(answers) and answers[i] != -1:
        return answers[i]
    elif i % 2 != 0:
        tempAns = collatz_single((i * 3 + 1) // 2) + 2
        if i < len(answers):
            answers[i] = tempAns
        return tempAns
    else:
        tempAns = collatz_single(i // 2) + 1
        if i < len(answers):
            answers[i] = tempAns
        return tempAns


# -------------
# collatz_print
# -------------


def collatz_print(w: IO[str], i: int, j: int, v: int) -> None:
    """
    print three ints
    w a writer
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    v the max cycle length
    """
    w.write(str(i) + " " + str(j) + " " + str(v) + "\n")


# -------------
# collatz_solve
# -------------


def collatz_solve(r: IO[str], w: IO[str]) -> None:
    """
    r a reader
    w a writer
    """
    for s in r:
        i, j = collatz_read(s)
        v = collatz_eval(i, j)
        collatz_print(w, i, j, v)
