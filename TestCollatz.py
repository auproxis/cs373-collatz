#!/usr/bin/env python3

# --------------
# TestCollatz.py
# --------------

# pylint: disable = bad-whitespace
# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import (
    collatz_read,
    collatz_eval,
    collatz_print,
    collatz_solve,
    collatz_single,
    answers,
)

# -----------
# TestCollatz
# -----------


class TestCollatz(TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 1)
        self.assertEqual(j, 10)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n"
        )

    # ----
    # My Unit Tests
    # ----
    def test_dao_1(self):
        self.assertEqual(collatz_single(1), 1)  # test base case

    def test_dao_2(self):
        self.assertEqual(collatz_single(2), 2)  # test first even

    def test_dao_3(self):
        self.assertEqual(collatz_single(16), 5)  # tests larger complex even

    def test_dao_4(self):
        self.assertEqual(collatz_single(3), 8)  # tests odd

    def test_dao_5(self):
        collatz_single(3)
        self.assertEqual(answers[3], 8)  # makes sure odds cache answers

    def test_dao_6(self):
        collatz_single(16)
        self.assertEqual(answers[16], 5)  # make sure evens caches

    def test_dao_7(self):
        collatz_single(22)
        self.assertEqual(
            answers[22], 16
        )  # make sure mixed even and odd in cycle caches

    def test_dao_8(self):  # make sure odd intermediate steps remain cached
        collatz_single(22)
        self.assertEqual(answers[11], 15)

    def test_dao_9(self):  # checks caching of even intermediates
        collatz_single(22)
        self.assertEqual(answers[10], 7)

    def test_dao_10(self):  # makes sure I can reverse the flow of the numbers
        v = collatz_eval(200, 100)
        self.assertEqual(v, 125)
    
    def test_dao_11(self):  # makes sure I can run collatz at boundries
        v = collatz_eval(1000000, 99999)
        self.assertEqual(v, 525)


# ----
# main
# ----

if __name__ == "__main__":  # pragma: no cover
    main()
